#[cfg(test)]
mod tests;

use std::ops::DerefMut;

pub trait HeapsortBy {
    type Item;
    fn heapsort_by<F: Fn(&Self::Item, &Self::Item) -> bool>(&mut self, f: F);
}
trait Heapsort {
    type Item;
    fn heapsort(&mut self);
}

pub trait IntrosortBy {
    type Item;
    fn introsort_by<F: Fn(&Self::Item, &Self::Item) -> bool>(&mut self, f: F);
}
trait Introsort {
    type Item;
    fn introsort(&mut self);
}

fn parent(i: usize) -> usize {
    (i-1)/2
}
fn left(i: usize) -> usize {
    2*i + 1
}
fn right(i: usize) -> usize {
    2*i + 2
}

fn sift_down<E, F: Fn(&E, &E) -> bool>(arr: &mut[E], start: usize, end: usize, f: F) {
    let mut root = start;
    while left(root) <= end {
        let l = left(root);
        let r = right(root);
        let mut swap = root;
        if f(&arr[swap], &arr[l]) {
            swap = l;
        }
        if r <= end && f(&arr[swap], &arr[r]) {
            swap = r;
        }
        if swap == root {
            return;
        } else {
            arr.swap(root, swap);
            root = swap;
        }
    }
}

fn build_heap<E, F: Fn(&E, &E) -> bool>(arr: &mut[E], f: F) {
    let end = arr.len() - 1;
    let start = parent(end);
    for i in (0..start).rev() {
        sift_down(arr, i, end, |a, b| f(a, b));
    }
}

fn heapsort_by<E, F: Fn(&E, &E) -> bool>(arr: &mut[E], f: F) {
    if arr.len() > 1 {
        build_heap(arr, |a, b| f(a, b));
        for i in (1..arr.len()).rev() { 
            arr.swap(i, 0);
            let end = i-1;
            sift_down(arr, 0, end, |a, b| f(a, b));
        }
    }
}

fn partition<E, F: Fn(&E, &E) -> bool>(sl: &mut[E], f: F) -> (&mut[E], &mut[E]) {
    let l = sl.len();
    let p = l / 2;
    let pp;
    let (mut i, mut j) = (0, l);
    loop {
        for x in i..l {
            i = x;
            if f(&sl[p], &sl[i]) {
                break;
            }
        }
        for x in (0..j).rev() {
            j = x;
            if !f(&sl[p], &sl[j]) {
                break;
            }
        }
        if j <= i {
            pp = j;
            break;
        }
        sl.swap(i, j);
    }
    let (x, y) = sl.split_at_mut(pp);
    let (_, y) = y.split_at_mut(1);
    (x, y)
}
fn isort_by<E>(arr: &mut[E], f: &Fn(&E, &E) -> bool, depth: usize, len: usize) { 
    if arr.len() <= 1 {
        return;
    }
    if depth > 5 && depth * arr.len() > len {
        heapsort_by(arr, |a, b| f(a, b));
        return;
    }
    let (x, y) = partition(arr, |a, b| f(a, b));
    isort_by(x, f, depth+1, len);
    isort_by(y, f, depth+1, len);
}

fn sort_by<E, F: Fn(&E, &E) -> bool>(arr: &mut[E], f: F) {
    let len = arr.len();
    isort_by(arr, &f, 1, len);
}


impl<A: DerefMut<Target=[E]>, E> HeapsortBy for A {
    type Item = E;
    fn heapsort_by<F: Fn(&E, &E) -> bool>(&mut self, f: F) {
        heapsort_by(self, f);
    }
}
impl<A: DerefMut<Target=[E]>, E: PartialOrd> Heapsort for A {
    type Item = E;
    fn heapsort(&mut self) {
        self.heapsort_by(|a, b| a < b);
    }
}

impl<A: DerefMut<Target=[E]>, E> IntrosortBy for A {
    type Item = E;
    fn introsort_by<F: Fn(&E, &E) -> bool>(&mut self, f: F) {
        sort_by(self, f);
    }
}
impl<A: DerefMut<Target=[E]>, E: PartialOrd> Introsort for A {
    type Item = E;
    fn introsort(&mut self) {
        self.introsort_by(|a, b| a < b);
    }
}
