use crate::*;

#[test]
fn test_sort_empty() {
    let mut v: Vec<String> = vec![];
    assert!(v.len() == 0);
    v.introsort();
    assert!(v.len() == 0);
}

#[test]
fn test_heapsort_empty() {
    let mut v: Vec<String> = vec![];
    assert!(v.len() == 0);
    v.heapsort();
    assert!(v.len() == 0);
}
