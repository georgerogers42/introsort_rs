#[macro_use]
extern crate clap;
#[macro_use]
extern crate lazy_static;
extern crate regex;
extern crate introsort;

#[cfg(test)]
pub mod tests;

use std::io;
use std::io::prelude::*;
use std::fmt;
use regex::Regex;
use clap::{Arg};
use introsort::*;

fn slurp<R: BufRead>(r: R) -> io::Result<Vec<String>> {
    let mut lines = vec![];
    for line in r.lines() {
        let line = line?;
        lines.push(line);
    }
    Ok(lines)
}

#[derive(Clone, PartialEq, Eq, PartialOrd, Ord, Debug)]
struct Verse { 
    book: String,
    chapter: u64,
    verse: u64,
    text: String,
}

lazy_static! {
    static ref VERSE: Regex = Regex::new(r"^(\w+)\s+(\d+):(\d+)\s+(.*)$").unwrap();
}

impl Verse {
    pub fn new(t: &str) -> Option<Verse> {
        VERSE.captures(t).map(|caps| {
            Verse { 
                book: caps[1].to_owned(), 
                chapter: caps[2].parse().unwrap(),
                verse: caps[3].parse().unwrap(),
                text: caps[4].to_owned(),
            }
        })
    }
}

impl fmt::Display for Verse {
    fn fmt(&self, w: &mut fmt::Formatter) -> fmt::Result {
        write!(w, "{} {}:{} {}", self.book, self.chapter, self.verse, self.text)
    }
}

fn sort_text<W: Write>(mut w: W, ls: &[String], rev: bool) -> io::Result<()> {
    let mut lines: Vec<&str> = ls.iter().map(|s| s as &str).collect();
    if rev {
        lines.introsort_by(|a, b| a > b);
    } else {
        lines.introsort_by(|a, b| a < b);
    }
    for line in lines {
        writeln!(w, "{}", line)?;
    }
    Ok(())
}
fn sort_verses<W: Write>(mut w: W, lines: &[String], rev: bool) -> io::Result<()> {
    let mut verses: Vec<Verse> = lines.iter().flat_map(|line| Verse::new(line)).collect();
    if rev {
        verses.introsort_by(|a, b| (&b.book, a.chapter, a.verse, &b.text) < (&a.book, b.chapter, b.verse, &a.text));
    } else {
        verses.introsort_by(|a, b| a < b);
    }
    for verse in verses {
        writeln!(w, "{}", verse)?;
    }
    Ok(())
}

fn main() {
    let matches = app_from_crate!()
        .arg(Arg::with_name("reverse").short("r").long("rev"))
        .arg(Arg::with_name("bible").short("b").long("bible"))
        .get_matches();
    let stdin = io::stdin();
    let stdout = io::stdout();
    let mut lines = slurp(stdin.lock()).unwrap();
    let rev = matches.is_present("reverse");
    let bible = matches.is_present("bible");
    if bible {
        sort_verses(stdout.lock(), &lines, rev).unwrap();
    } else {
        sort_text(stdout.lock(), &mut lines, rev).unwrap();
    }
}
